import Vue from 'vue'
import App from './App.vue'
import VueRouter from "vue-router";
import router from "./router";
import Vant from 'vant'

Vue.config.productionTip = false



// 现在，应用已经启动了！

Vue.use(VueRouter);
Vue.use(Vant);
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
