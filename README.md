# FunnyFTP

#### 介绍
使用http的协议实现了远程的文件管理，通用所有系统，基于jdk1.8
目前提供的功能：文件的**浏览，压缩，解压缩，下载，删除，新建文件夹，上传文件；文件上传大小限制，远程文件管理根目录设置** 。
** 。
<br />
<a href='https://gitee.com/haust_lyb/HTTPFTP'><img src='https://gitee.com/haust_lyb/HTTPFTP/widgets/widget_3.svg' alt='Fork me on Gitee'></img></a>
<br />



最新版下载地址：https://gitee.com/haust_lyb/HTTPFTP/attach_files/704516/download/fileserver-version-1.1.zip



#### 安装教程

1. 根据上方连接地址下载的zip包 （有版本区别），在计算机上解压缩 windows双击start.bat即可启动，Mac或者Windows需要运行start.sh
![下载解压后文件列表](https://images.gitee.com/uploads/images/2019/0626/172350_b68eef1d_939739.png "屏幕截图.png")
2. 启动后访问localhost:94进入应用主页 默认密码在解压后的config包里的文件中可以配置 默认是111111 输入密码错误不会提示 直到输入正确为止
![输入密码](https://images.gitee.com/uploads/images/2019/0626/173144_53cba577_939739.png "屏幕截图.png")
3. 相关截图
![主页面](https://images.gitee.com/uploads/images/2019/0627/121936_5f331f63_939739.png "屏幕截图.png")
<br />
注意：⚠️ 启动脚本可能会遇到权限问题 可以自己去添加sudo之类 脚本的本质也就是进入jar包所在文件夹 运行 java -jar fileserver-0.0.1-SNAPSHOT.jar

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 遇到问题
1.去邮箱留言，地址：1570194845@qq.com
2.直接在issue中提出或者评论中提出
